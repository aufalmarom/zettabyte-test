// Expected result :  [7, 7, 7, 6, 92, 11]
// Direction : copy arr1 last three of element without removing default length and fill the rest value with 7

const arr1 = [12, 24, 51, 6, 92, 11];

function result(arr1) {
  for (let index = 0; index < arr1.length; index++) {
    const element = arr1[index];
    if (index < 3) {
      delete arr1[index];
    }
  }
  const newArray = arr1;
  for (let index = 0; index < newArray.length; index++) {
    const element = newArray[index];
    if (newArray[index] === undefined) {
      newArray[index] = 7;
    }
  }
  console.log(`newArray : `, newArray);
  return newArray;
}

console.log(result(arr1));
