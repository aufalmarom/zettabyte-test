// Expected Result = 7
// Direction : Find median of this array
const input = [8, 7, 7, 9, 5, 4, 2, 9];

function result(input) {
  const middleValue = Math.floor(input.length / 2);
  const sortArray = [...input].sort((a, b) => a - b);
  console.log(`middleValue : `, middleValue);
  console.log(`sortArray : `, sortArray);

  const resultMedian =
    input.length % 2 !== 0
      ? sortArray[middleValue]
      : (sortArray[middleValue - 1] + sortArray[middleValue]) / 2;
  console.log(`resultMedian : `, resultMedian);

  return resultMedian;
}

console.log(result(input));
