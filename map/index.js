// Expected Result :
// [
//   { name: 'Rian Nugraha', school_name: 'PLMK-JKT' },
//   { name: 'Ari Santoso', school_name: 'GRSR-JKT' },
//   { name: 'Rahman Sunggara', school_name: 'GELM-JKT' },
// ]
// Direction :
// Return a formatted array for students of specific school using id of school.

const source = [
  {
    id: '1',
    data: {
      first_name: 'Rian',
      last_name: 'Nugraha',
    },
    school: {
      id: '1',
      data: 'PLMK-JKT',
    },
  },
  {
    id: '2',
    full_name: 'Ari Santoso',
    school: {
      id: '1',
      short_name: 'GRSR',
      data: 'JKT',
    },
  },
  {
    id: '3',
    data: {
      first_name: 'Rahman',
      last_name: 'Sunggara',
    },
    school: {
      id: '1',
      short_name: 'GELM',
      data: 'JKT',
    },
  },
  {
    id: '4',
    data: {
      first_name: 'Rian',
      last_name: 'Nugraha',
    },
    school: {
      id: '2',
      data: 'PLMK-BDG',
    },
  },
];

function result(num) {
  let datas = num.map((element) => {
    const data = new Array();
    if (element.full_name) {
      data.push({
        name: element.full_name,
        school_name: `${element.school.short_name}-${element.school.data}`,
      });
    } else {
      if (element.school.short_name) {
        data.push({
          name: `${element.data.first_name} ${element.data.last_name}`,
          school_name: `${element.school.short_name}-${element.school.data}`,
        });
      } else {
        data.push({
          name: `${element.data.first_name} ${element.data.last_name}`,
          school_name: element.school.data,
        });
      }
    }
    return data[0];
  });
  const seen = new Set();
  const newDatas = datas.filter((el) => {
    const res = seen.has(el.name);
    seen.add(el.name);
    return !res;
  });
  return newDatas;
}

console.log(result(source));
