// Expected Result : [
//   { name: "Susanti", id: "1", score: 4, status_test: "fresher" },
//   { name: "Mei", id: "2", score: 7, status_test: "junior" },
//   { name: "Ros", id: "3", score: 9, status_test: "senior" },
//   { name: "Mail", id: "4", score: 2, status_test: "fresher" },
// ];

// Direction : Please specify the test result status for each candidate (arr2) based on indicator (arr1)
// Note : arr1 must be used

const arr1 = [
  { min_score: 1, status: 'fresher' },
  { min_score: 5, status: 'junior' },
  { min_score: 8, status: 'senior' },
];

const arr2 = [
  { name: 'Susanti', id: '1', score: 4, status_test: '' },
  { name: 'Mei', id: '2', score: 7, status_test: '' },
  { name: 'Ros', id: '3', score: 9, status_test: '' },
  { name: 'Mail', id: '4', score: 2, status_test: '' },
];

function result(arr1, arr2) {
  for (let index = 0; index < arr2.length; index++) {
    const element = arr2[index];
    const val = element.score;
    const newStatus = new Array();
    for (let index = 0; index < arr1.length; index++) {
      const elm = arr1[index];
      if (val > elm.min_score) {
        newStatus.push(elm.status);
      }
    }
    const latestStatus = newStatus[newStatus.length - 1];
    element.status_test = latestStatus;
  }
  return arr2;
}

console.log(result(arr1, arr2));
