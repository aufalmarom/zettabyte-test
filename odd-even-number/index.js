// Expected result : [[1, 3, 5, 7, 9], [2, 4, 6, 8, 10]]
// Direction : Return nested array first is odd value and second is even array from the array number
const number = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function result(number) {
  const newArray = new Array();
  const odd = number.filter((el) => {
    return el % 2 !== 0;
  });
  newArray.push(odd);
  const even = number.filter((el) => {
    return el % 2 === 0;
  });
  newArray.push(even);
  console.log(`odd`, odd);
  console.log(`even`, even);
  console.log(`newArray`, newArray);
  return newArray;
}

console.log(result(number));
